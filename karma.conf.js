module.exports = function(config) {
  var customLaunchers = {
    sl_chrome: {
      base: 'SauceLabs',
      browserName: 'chrome',
      platform: "Linux",
    },
    sl_firefox: {
      base: 'SauceLabs',
      browserName: 'firefox',
      version: '51',
    },
    // sauce labs why soooo old :(
    //sl_android: {
    //  base: 'SauceLabs',
    //  browserName: 'android',
    //},
  }

  config.set({
    sauceLabs: {
        testName: 'Web App Unit Tests'
    },
    customLaunchers: customLaunchers,
    browsers: Object.keys(customLaunchers),
    reporters: ['dots', 'saucelabs'],
    files: [
      { pattern: 'index.js', watched: false }
    ],
    frameworks: ['jasmine'],
    preprocessors: {
      'index.js': ['webpack'],
    },
    singleRun: true,
    webpack: {
      module: {
        loaders: [
          { test: /\.tsx?$/, loader: 'ts-loader' }
        ],
      },
      watch: true,
      resolve: {
        extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js']
      },
    },
    webpackServer: {
      noInfo: true,
    },
    browserDisconnectTimeout: 10000,
    browserDisconnectTolerance: 1,
    browserNoActivityTimeout: 6*60*1000,
    captureTimeout: 6*60*1000,
    colors: true,
    logLevel: config.LOG_INFO
  });
};

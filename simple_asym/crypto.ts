// Use this lighter polyfill for dumb safari
import { crypto_pwhash, crypto_pwhash_SALTBYTES } from "libsodium-wrappers";
import {TextDecoder, TextEncoder} from "text-encoding-utf-8";
import { HASH_MEMORY } from "./constants";
import sodium from "./sodium";

export interface IKeyPair {
    keyType?: string;
    privateKey: Uint8Array;
    publicKey: Uint8Array;
}

export { default as sodium } from "./sodium";

export type DefaultBinary = Uint8Array;

export function toBase64(input: DefaultBinary): string {
    return sodium.to_base64(input, sodium.base64_variants.ORIGINAL);
}

export function fromBase64(input: string): DefaultBinary {
    return sodium.from_base64(input, sodium.base64_variants.ORIGINAL);
}

const enc = new TextEncoder();
const dec = new TextDecoder();

export function toUTF8(input: DefaultBinary): string {
    return dec.decode(input);
}

export function fromUTF8(input: string): DefaultBinary {
    return enc.encode(input);
}
export function generateKeys(): IKeyPair {
    return sodium.crypto_box_keypair();
}

// https://stackoverflow.com/a/14071518/907060
function concatenate(a: Uint8Array, b: Uint8Array ): Uint8Array {
  const c = new Uint8Array(a.length + b.length);
  c.set(a);
  c.set(b, a.length);
  return c;
}

interface IHashVersion {
    OPSLIMIT: number;
    MEMLIMIT: number;
    ALG: number;
}

/**
 * Version 1 just the default interactive from libsodium.js 0.5.x
 *
 * Version 2 is based on libsodium.js 0.7.x but deviates to conserve ram
 * This is because web assembly is unable to correctly grow ram as needed.
 * Timing on i7-8550U is about 700ms in Firefox and 1200ms in Chrome.
 */
const hashVersions: {[version: number]: IHashVersion} = {
    1: {
        OPSLIMIT: 4,
        MEMLIMIT: HASH_MEMORY,
        ALG: 1,  // Argon2i
    },
    2: {
        OPSLIMIT: 5,  // Above default
        MEMLIMIT: HASH_MEMORY,  // Below default
        ALG: 2,  // Argon2id
    },
};

export function hashPassword(password: string, salt: DefaultBinary, version = 1): DefaultBinary  {
    // console.log(sodium.crypto_box_SEEDBYTES);
    // console.log(sodium.crypto_pwhash_OPSLIMIT_INTERACTIVE);
    // console.log(sodium.crypto_pwhash_MEMLIMIT_INTERACTIVE);
    // console.log(sodium.crypto_pwhash_ALG_DEFAULT);
    const hashVersion = hashVersions[version];
    if (!hashVersion) {
        throw Error("Invalid hash version");
    }
    return sodium.crypto_pwhash(
        sodium.crypto_box_SEEDBYTES,
        password,
        salt,
        hashVersion.OPSLIMIT,
        hashVersion.MEMLIMIT,
        hashVersion.ALG,
    );
}

export function makeSalt(): DefaultBinary {
    return sodium.randombytes_buf(sodium.crypto_pwhash_SALTBYTES);
}

/** Generate easy to type truly random string */
export function makeRandomTypableString(numberToGenerate = 32) {
    const allowedChars = "BCDFGHJKMQRTVWXY2346789";
    let result = "";
    for (let i = 0; i < numberToGenerate; i++) {
        result += allowedChars[sodium.randombytes_uniform(allowedChars.length)];
    }
    return result;
}

export function encrypt(key: DefaultBinary, message: DefaultBinary): DefaultBinary {
    const nonce = sodium.randombytes_buf(sodium.crypto_secretbox_NONCEBYTES);
    return concatenate(nonce, sodium.crypto_secretbox_easy(message, nonce, key));
}

export function decrypt(key: DefaultBinary, ciphertext: DefaultBinary): DefaultBinary {
    const nonce = ciphertext.slice(0, sodium.crypto_secretbox_NONCEBYTES);
    const encryptedMessage = ciphertext.slice(sodium.crypto_secretbox_NONCEBYTES);
    return sodium.crypto_secretbox_open_easy(encryptedMessage, nonce, key);
}

/**
 * Encrypt the private key in the format [salt, ciphertext]
 * @param privateKey - private key
 * @param password - human typable password for symmetrically encrypting private key
 */
export function encryptPrivateKey(privateKey: DefaultBinary, password: string): DefaultBinary {
  const salt = makeSalt();
  const version = 2;
  const hash = hashPassword(password, salt, version);
  const ciphertext = encrypt(hash, privateKey);
  // format is hash version|salt|ciphertext
  return concatenate(
      concatenate(new Uint8Array([version]), salt),
      ciphertext,
  );
}

/**
 * Decrypt the private key
 * @param privateKey - encrypted ciphertext of private key.
 * @param password - human typeable (short) password for decrypting private key
 */
export function decryptPrivateKey(privateKey: DefaultBinary, password: string): DefaultBinary {
  // If no version identifer then it's version 1
  let version = 1;
  if (privateKey.byteLength > 88) {
    // First byte is a uin8 that represents the version
    version = privateKey[0];
    if (version !== 2) {
        throw Error("Invalid private key passit pw_hash version");
    }
    privateKey = privateKey.slice(1);
  }
  const salt = privateKey.slice(0, sodium.crypto_pwhash_SALTBYTES);
  const ciphertext = privateKey.slice(sodium.crypto_pwhash_SALTBYTES);
  const hash = hashPassword(password, salt, version);
  return decrypt(hash, ciphertext);
}

export function generateSymmetricKey(): Uint8Array {
    return sodium.randombytes_buf(sodium.crypto_secretbox_KEYBYTES);
}

/** Encrypt a plaintext message using my private key and their public key
 * Keys are accepted as both base64 strings and Uint8Array sodium.DefaultBinary.
 * Returns ciphertext concatenated with new generated nonce
 */
export function rsaEncrypt(theirPublicKey: DefaultBinary, plaintext: DefaultBinary): DefaultBinary {
    return sodium.crypto_box_seal(plaintext, theirPublicKey);
}

/** Decrypt cyphertext using it's creator public key and our own private key
 * Keys and encryptedBuffer are accepted as both base64 strings and Uint8Array sodium.DefaultBinary.
 * Returns decrypted message
 */
export function rsaDecrypt(
    myPublicKey: DefaultBinary,
    myPrivateKey: DefaultBinary,
    encryptedBuffer: DefaultBinary,
): DefaultBinary {
  return sodium.crypto_box_seal_open(encryptedBuffer, myPublicKey, myPrivateKey);
}

export function authenticatedEncryption(publicKey: DefaultBinary, privateKey: DefaultBinary, message: DefaultBinary) {
    const nonce = sodium.randombytes_buf(sodium.crypto_box_NONCEBYTES);
    return concatenate(nonce, sodium.crypto_box_easy(message, nonce, publicKey, privateKey));
}

function authenticatedDecryption(publicKey: DefaultBinary, privateKey: DefaultBinary, ciphertext: DefaultBinary) {
    const nonce = ciphertext.slice(0, sodium.crypto_box_NONCEBYTES);
    const encryptedMessage = ciphertext.slice(sodium.crypto_secretbox_NONCEBYTES);
    return sodium.crypto_box_open_easy(encryptedMessage, nonce, publicKey, privateKey);
}

/** Verify the public key is the keypair of a potential private key owner.
 * Use Case:
 * Sally has a private key that is unknown to our server.
 * Our server wishes to know if user Sally has access to  Sally's private key but does not want to know the private key
 * Sally can encrypt a message using the server's public key and send the encrypted message to the server.
 * The server can decrypt the message and verify claimed user Sally must in fact have access to Sally's public key.
 *
 * Why not sign and verify signature?
 * Libsodium signatures require a longer key length than the key pairs generated for crypto_box_seal.
 *
 * Returns true if public key owner is verified. False if otherwise.
 */
export function verifyOwner(publicKey: DefaultBinary, privateKey: DefaultBinary, ciphertext: DefaultBinary) {
    try {
        authenticatedDecryption(publicKey, privateKey, ciphertext);
    } catch (err) {
        return false;
    }
    // Anything that doesn't error is success because we are only verifying the sender
    return true;
}

# Changelog

## 0.6.1

Switch to non sumo libsodium.js version as it now contains pw_hash and reduce memory usage. 
This update should greatly reduce both file size and memory!

## 0.6.0

Use text-encoding-utf-8 polyfoll fork so reduce file size.

## 0.3.0

* Refactor to provide better type safety ([#1](https://gitlab.com/burke-software/passit/simple-asymmetric-js/merge_requests/1))

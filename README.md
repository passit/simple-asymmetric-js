The original goal of this project was to make an easier to use wrapper around libsodium for various platforms.
It has become instead the Passit libsodium wrapper as it's very catered towards the usage in Passit. Feel free to use it for anything though.

Why not just use libsodium? While libsodium is great - it is hard to get started with and the various language bindings are often incompatible and difficult to use. For example the js bindings compared to the Java or Python bindings.

Libsodium provides good defaults if you already know how encryption works and just aren't sure which defaults to use. simple-asymmetric attempts to provide a easy to use solution for sharing data over an untrusted medium.
It also saves data as base64 strings to easy storage.

That said for most cases SSL is what you want for sending over the internet. But what if we want to store our messages on an evil server?

Let's say Bob wants to share a message with Alice. Bob needs to store his message on an untrusted server for it to get to Alice.

We can use asymmetric encryption (also known as RSA encryption or Public key encryption) to send a message to someone we never spoke with before over an untrusted medium.
However it is slow and not suitable for long messages.
We can use symmetric encryption (also known as Secret Key encryption) to send messages to anyone who shared the same secret key. It's fast and suitable for long messages.

If we want to send long messages to someone we haven't met before - we can use both!

Let's say Bob wants to send Alice a long message. For Alice to get the message, it must be stored on an evil server that wants to read everyone's data!

```typescript
import {sodium, KeyPair, SymmetricKey, UTF8Binary} from "simple-asymmetric-js";

// Wait for libsodium.js to become ready
await sodium.ready;

// Generate key pairs for Bob and for Alice
const bob = KeyPair.generate();
const alice = KeyPair.generate();

// Generate one secret key for symmetric encryption
const bobSymmetricKey = SymmetricKey.generate();

// Send key to alice ༼つ◕_◕ ༽つ
const encryptedSymmetricKey = alice.publicKey.encrypt(bobSymmetricKey);
// Maybe encryptedSymmetricKey is saved in a database owned by Evil Server.
// Evil Server is watching encryptedSymmetricKey
// But can't figure out what it is
// Alice can decrypt the secret key now, with her private key
const aliceSymmetricKey = alice.decrypt(encryptedSymmetricKey, SymmetricKey);

// Now that alice has the secret key, we can send a long message
const ciphertext = bobSymmetricKey.encrypt(new UTF8Binary("Do you know what cryptobox means?"));
// Evil server is still watching....maybe as bob saves it to that database again

// Alice can decrypt the message. Perhaps this is happening locally and not
// where Evil Server can watch
const decryptedMessage = aliceSymmetricKey.decrypt(ciphertext, UTF8Binary);
console.log(decryptedMessage.string);
 >> "Do you know what cryptobox means?";

// Alice can send back data easy now without RSA encryption.
// She could also choose to generate a new RSA key instead and start over.
const ciphertext2 = aliceSymmetricKey.encrypt(new UTF8Binary("Yes but why is crypto so hard to use?"));
```

# Status

API not yet stable. You shouldn't use this yet.

# Developing

We use webpack, docker, typescript, and jasmine.

1. Install Docker and Docker Compose
2. `docker-compose up`
3. Go to http://localhost:8080/ to view passing tests

## CI

Saucelabs is used for CI. See karma.conf.js and .gitlab-ci.yml

Everything runs from Docker - so you should be able to run something like

1. `docker-compose run --rm app bash`
2. Set saucelab env vars (username and access key)
3. `karma start`

# How

Both Asymmetrical and Symmetrical encryption use libsodium.js.

Binary data is converted to base64 for easy storage.

# Supported platforms

Tested against latest Chrome and Firefox. Should also run in node (needs tested).

You need to install the `text-encoding` polyfil package if your browser
doesn't support it.


# Ports

- [Python](https://gitlab.com/burke-software/simple-asymmetric-python). This port uses an older API and is not canonical.

# Contributing

Merge requests require tests.

No core features will be accepted without discussion and inclusion on other ports

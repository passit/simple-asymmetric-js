FROM node:8

RUN mkdir /code
WORKDIR /code

# Install Node deps
RUN yarn global add webpack webpack-dev-server typings karma-cli
COPY package.json /code/package.json
RUN yarn

VOLUME /code/node_modules

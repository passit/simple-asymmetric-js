var jasmineWebpackPlugin = require('jasmine-webpack-plugin');

module.exports = {
  entry: './simple_asym/test.ts',
  output: {
    filename: 'bundle.js'
  },
  resolve: {
    // Add `.ts` and `.tsx` as a resolvable extension. 
    extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js']
  },
  module: {
    rules: [{
        test: /\.ts(x?)$/,
        use: [
            {loader: 'ts-loader'}
        ]
    }]
  },
  plugins: [new jasmineWebpackPlugin({filename: "index.html"})],
  node: {
    fs: "empty"
  }
}
